# -*- coding: utf-8 -*-
from django.urls import path

from .views import *

urlpatterns = [
    path('notices', NoticeList.as_view()),
    path('config', AppConfig.as_view()),
]

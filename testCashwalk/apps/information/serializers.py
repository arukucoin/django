# -*- coding: utf-8 -*-
from rest_framework import serializers

from .models import *

class NoticeSerializer(serializers.ModelSerializer):

    class Meta:
        model = Notice
        fields = ('title', 'description', 'published')
        read_only_fields = ('title', 'description', 'published')

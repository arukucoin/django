from django.db import models
from django.conf import settings

# Create your models here.

class Notice(models.Model):
    title = models.CharField(
        max_length=255,
        blank=True,
        default="",
    )
    description = models.TextField(
        max_length=2000,
        blank=True,
        default="",
    )
    published = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ('-id',)
        verbose_name = 'Notice'
        verbose_name_plural = 'Notices'

    def __str__(self):
        return self.title

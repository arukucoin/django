# -*- coding: utf-8 -*-
from django.conf import settings

from rest_framework import parsers, renderers, generics, status, authentication
from rest_framework.views import APIView
from rest_framework.response import Response

from .serializers import *

from testCashwalk.utils import ResultPagination


class NoticeList(generics.ListAPIView):
    renderer_classes = (renderers.JSONRenderer,)
    pagination_class =  ResultPagination
    serializer_class = NoticeSerializer

    def get_queryset(self):
        return Notice.objects.filter()


class AppConfig(APIView):
    parser_classes = (parsers.JSONParser,)
    renderer_classes = (renderers.JSONRenderer,)

    def get(self, request, *args, **kwargs):
        return Response({'max_step_coin': settings.MAXIMUM_DAILY_STEP_COIN,
            'coin_per_step': settings.COIN_PER_STEP,
            'registration_coin': settings.REGISTRATION_REWARD_COIN,
            'invitation_coin': settings.INVITATION_REWARD_COIN,
            'step_ad_interval': settings.STEP_COIN_AD_INTERVAL,
            'max_invitation_people': settings.MAXIMUM_INVITATION_PEOPLE
            },
            status=status.HTTP_200_OK)

from django.contrib import admin

from .models import *

class NoticeAdmin(admin.ModelAdmin):
    model = Notice
    list_display = ["__str__", 'published']
    ordering = ('-pk',)

admin.site.register(Notice, NoticeAdmin)

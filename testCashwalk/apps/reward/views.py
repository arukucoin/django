# -*- coding: utf-8 -*-
from django.conf import settings
from django_filters.rest_framework import DjangoFilterBackend
from django.core.exceptions import ObjectDoesNotExist
from django.http import Http404

from rest_framework import parsers, renderers, generics, status, authentication
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated

from testCashwalk.utils import ResultPagination
from testCashwalk.apps.action.models import ConsumptionAction
from testCashwalk.apps.inventory.models import Gift

from .models import *
from .serializers import *

from datetime import datetime, timedelta


class BrandGiftList(generics.ListAPIView):
    parser_classes = (parsers.JSONParser,)
    renderer_classes = (renderers.JSONRenderer,)
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    pagination_class =  ResultPagination
    serializer_class = BrandGiftSerializer

    def get_queryset(self):
        return Brand.objects.filter(is_active=True)


class ProductList(generics.ListAPIView):
    parser_classes = (parsers.JSONParser,)
    renderer_classes = (renderers.JSONRenderer,)
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    pagination_class =  ResultPagination
    serializer_class = ProductSerializer

    # def list(self, request, *args, **kwargs):
        # response = super(ProductList, self).list(request, *args, **kwargs)
        # brand_id = self.request.query_params.get('id', None)
        #
        # if last_post_id is None and self.request.method == 'GET':
        # 	response.data['description'] = self.request.user.profile.school.name
        # return response

    def get_queryset(self):
        brand_id=self.kwargs['id']
        return Product.objects.filter(brand__id=brand_id, product_type=settings.SHOPPING, is_active=True)


class LotteryProductList(generics.ListAPIView):
    queryset = Product.objects.all()
    parser_classes = (parsers.JSONParser,)
    renderer_classes = (renderers.JSONRenderer,)
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    pagination_class =  ResultPagination
    serializer_class = LotteryProductSerializer

    def get_queryset(self):
        return Product.objects.filter(product_type=settings.LOTTERY, is_active=True)


class PurchaseProduct(APIView):
    parser_classes = (parsers.JSONParser,)
    renderer_classes = (renderers.JSONRenderer,)
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get(self, request, uid, format=None):
        try:
            product = Product.objects.get(uid=uid)

            if self.request.user.profile.wallet.current_total_coin >= product.price and product.is_active == True:

                time_threshold = datetime.now() + timedelta(days=settings.GIFT_EXPIRATION_DAYS)
                gift = Gift.objects.filter(product_uid=product.uid, is_published=False, expiration_date__gt=time_threshold).first()
                if gift:
                    expiration = datetime.now() + timedelta(days=7, hours=1)
                    ConsumptionAction.objects.create(profile=self.request.user.profile,
                        type=product.product_type, coin=product.price, product=product)
                    ticket = Ticket.objects.create(product=product, profile=self.request.user.profile, ticket_url=gift.url)
                    gift.published(ticket.uid)
                    self.request.user.profile.wallet.buy_product(product.price)

                    return Response(status=status.HTTP_204_NO_CONTENT)

            return Response(status=status.HTTP_400_BAD_REQUEST)

        except ObjectDoesNotExist:
            raise Http404

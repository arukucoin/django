from django.contrib import admin

from .models import *


class BrandAdmin(admin.ModelAdmin):
    model = Brand
    list_display = ['__str__', 'is_image', 'is_active']
    ordering = ('-pk',)

admin.site.register(Brand, BrandAdmin)

class ProductAdmin(admin.ModelAdmin):
    model = Product
    list_display = ['brand', '__str__', 'uid', 'is_image', 'price', 'product_type', 'description', 'expiration_days', 'is_active']
    ordering = ('-pk',)

admin.site.register(Product, ProductAdmin)

class TicketAdmin(admin.ModelAdmin):
    model = Ticket
    list_display = ['profile', '__str__', 'published_date', 'ticket_url', 'expiration_date', 'is_used']
    ordering = ('-pk',)

admin.site.register(Ticket, TicketAdmin)

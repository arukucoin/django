# -*- coding: utf-8 -*-
from django.urls import path

from .views import *

urlpatterns = [
    path('brands', BrandGiftList.as_view()),
    path('brands/<int:id>', ProductList.as_view()),
    path('lottery', LotteryProductList.as_view()),
    path('<uuid:uid>', PurchaseProduct.as_view()), # 1. Buy, 2. Lottery
]

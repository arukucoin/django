# -*- coding: utf-8 -*-
from rest_framework import serializers

from .models import *


class ProductSerializer(serializers.ModelSerializer):
    brandUrl = serializers.CharField(source='brand.brandUrl', read_only=True)
    brandname = serializers.CharField(source='brand.name', read_only=True)
    #description = serializers.CharField(source='brand.description', read_only=True)

    class Meta:
        model = Product
        fields = ('uid', 'brandname', 'brandUrl', 'name', 'imageUrl', 'price', 'description', 'expiration_days')
        read_only_fields = ('uid', 'brandname', 'brandUrl', 'name', 'imageUrl', 'price', 'description', 'expiration_days')


class BrandGiftSerializer(serializers.ModelSerializer):
    products = ProductSerializer(read_only=True, many=True, source='brand_product')

    class Meta:
        model = Brand
        fields = ('name', 'description', 'annotation', 'products')


class BulkProductSerializer(ProductSerializer):

    class Meta:
        model = Product
        fiedls = ('brandname', 'name', 'imageUrl')
        exclude = ('id', 'uid', 'brand', 'price', 'image', 'description', 'expiration_days', 'is_active', 'product_type')


class LotteryProductSerializer(ProductSerializer):

    class Meta:
        model = Product
        fields = ('uid', 'brandname', 'brandUrl', 'name', 'imageUrl', 'price', 'description',)


class TicketSerializer(serializers.ModelSerializer):
    brandName = serializers.CharField(source='product.brand.name', read_only=True)
    brandUrl = serializers.CharField(source='product.brand.brandUrl', read_only=True)
    productName  = serializers.CharField(source='product.name', read_only=True)
    annotation = serializers.CharField(source='product.brand.annotation', read_only=True)
    imageUrl = serializers.CharField(source='product.imageUrl', read_only=True)

    class Meta:
        model = Ticket
        fields = ('brandName', 'brandUrl', 'annotation', 'productName', 'imageUrl', 'ticketUrl', 'published_date', 'expiration_date')

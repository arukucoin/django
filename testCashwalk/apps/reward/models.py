from django.db import models
from django.conf import settings
from django.utils import timezone

from datetime import timedelta, datetime

from testCashwalk.utils import PathAndRename

import uuid

path_and_rename_brand = PathAndRename("brand_image")
path_and_rename_product = PathAndRename("product_image")


class Brand(models.Model):
    uid = models.UUIDField(
        unique=True,
        editable=False,
        default=uuid.uuid4,
        verbose_name='Brand identifier',
        primary_key=True,
    )
    name = models.CharField(max_length=100)
    description = models.TextField(
        max_length=2000,
        blank=True,
    )
    annotation = models.TextField(
        max_length=1000,
        blank=True,
    )
    image = models.ImageField(
        upload_to=path_and_rename_brand,
        blank=True,
        null=True,
    )
    is_active = models.BooleanField(default=True)

    class Meta:
        #ordering = ('-id',)
        verbose_name = 'Brand'
        verbose_name_plural = 'Brands'

    def __str__(self):
        return self.name

    def is_image(self):
        return True if self.image else False

    def imageUrl(self):
        if self.image:
            if settings.DEBUG == False:
                # return settings.AWS_CF_DOMAIN + self.image.name
                return self.image.url
            else:
                return self.image.url
        return ""

class Product(models.Model):
    brand = models.ForeignKey(
        Brand,
        related_name='brand_product',
        on_delete=models.CASCADE,
    )
    uid = models.UUIDField(
        unique=True,
        editable=False,
        default=uuid.uuid4,
        verbose_name='Product identifier',
        primary_key=True,
    )
    name = models.CharField(max_length=100)
    price = models.PositiveSmallIntegerField(default=0)
    description = models.TextField(
        max_length=2000,
        blank=True,
    )
    expiration_days = models.PositiveSmallIntegerField(default=0)
    image = models.ImageField(
        upload_to=path_and_rename_product,
        blank=True,
        null=True,
    )
    product_type = models.CharField(
        max_length=30,
        choices=settings.PRODUCT_TYPES,
        default=settings.SHOPPING,
    )
    is_active = models.BooleanField(default=True)

    class Meta:
        #ordering = ('-id',)
        verbose_name = "Product"
        verbose_name_plural = "Products"

    def __str__(self):
        return self.name

    def imageUrl(self):
        if self.image:
            if settings.DEBUG == False:
                # return settings.AWS_CF_DOMAIN + self.image.name
                return self.image.url
            else:
                return self.image.url

        return ""

    def is_image(self):
        return True if self.image else False


class Ticket(models.Model):
    uid = models.UUIDField(
        unique=True,
        editable=False,
        default=uuid.uuid4,
        verbose_name='Ticket identifier',
        primary_key=True,
    )
    product = models.ForeignKey(
        Product,
        related_name='set_tickets',
        on_delete=models.PROTECT,
    )
    profile = models.ForeignKey(
        'account.Profile',
        related_name='tickets',
        on_delete=models.CASCADE,
        #on_delete=models.PROTECT,
    )
    ticket_url = models.CharField(default="", max_length=255)
    is_used = models.BooleanField(default=False)
    published_date = models.DateTimeField(auto_now_add=True)

    class Meta:
        #ordering = ('-id',)
        verbose_name = "Ticket"
        verbose_name_plural = "Tickets"

    @property
    def expiration_date(self):
        return self.published_date + timedelta(days=7, hours=1)

    def ticketUrl(self):
        return self.ticket_url

    def __str__(self):
        return self.product.name

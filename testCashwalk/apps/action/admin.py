# -*- coding: utf-8 -*-
from django.contrib import admin

from .models import *


class ConsumptionActionAdmin(admin.ModelAdmin):
    model = ConsumptionAction
    list_display = ['__str__', 'type', 'coin', 'product', 'created']
    ordering = ('-pk',)

admin.site.register(ConsumptionAction, ConsumptionActionAdmin)


class RewardActionAdmin(admin.ModelAdmin):
    model = RewardAction
    list_display = ['__str__', 'type', 'coin', 'invitation_user', 'created'] #'date',
    ordering = ('-pk',)

admin.site.register(RewardAction, RewardActionAdmin)

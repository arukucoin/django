# -*- coding: utf-8 -*-
from django.urls import path

from .views import *

urlpatterns = [
    path('reward', RewardAction.as_view()),
    path('consumption', ConsumtionAction.as_view()),
]

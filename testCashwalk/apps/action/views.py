# -*- coding: utf-8 -*-
from rest_framework import parsers, renderers, generics, status, authentication
from rest_framework.permissions import IsAuthenticated

from .serializers import *

from testCashwalk.utils import ResultPagination


class RewardAction(generics.ListAPIView):
    queryset = RewardAction.objects.all()
    parser_classes = (parsers.JSONParser,)
    renderer_classes = (renderers.JSONRenderer,)
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    pagination_class =  ResultPagination
    serializer_class = RewardActionSerializer

    def get_queryset(self):
        return self.request.user.profile.reward_actions.all()


class ConsumtionAction(generics.ListAPIView):
    queryset = ConsumptionAction.objects.all()
    parser_classes = (parsers.JSONParser,)
    renderer_classes = (renderers.JSONRenderer,)
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    pagination_class =  ResultPagination
    serializer_class = ConsumptionActionSerializer

    def get_queryset(self):
        return self.request.user.profile.consumtion_actions.all()

# -*- coding: utf-8 -*-
from django.db import models
from django.conf import settings
from django.core.validators import MaxValueValidator, MinValueValidator

from testCashwalk.apps.reward.models import Product
from testCashwalk.utils import *


class CoinAction(models.Model):

    profile = models.ForeignKey(
        'account.Profile',
        on_delete=models.CASCADE, #models.PROTECT,
    )
    coin = models.PositiveSmallIntegerField(
        default=0,
    )
    created = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class RewardAction(CoinAction):

    profile = models.ForeignKey(
        'account.Profile',
        related_name='reward_actions',
        on_delete=models.CASCADE #models.PROTECT,
    )
    coin = models.PositiveSmallIntegerField(
        default=0,
        validators=[MinValueValidator(0), MaxValueValidator(settings.MAXIMUM_DAILY_STEP_COIN)],
    )
    type = models.CharField(
        max_length=30,
        choices=settings.REWARD_TYPES,
        blank=True,
    )
    invitation_user = models.CharField(
        max_length=100,
        default="",
        blank=True,
    )
    #date = models.DateTimeField(blank=True, null=True, editable=False)

    class Meta:
        ordering = ('-id',)
        verbose_name = 'Reward Action'
        verbose_name_plural = 'Reward Actions'
        get_latest_by = 'created'

    def __str__(self):
        return self.profile.user.username + "'s reward action"


class ConsumptionAction(CoinAction):

    profile = models.ForeignKey(
        'account.Profile',
        related_name='consumtion_actions',
        on_delete=models.CASCADE, #models.PROTECT,
    )
    product = models.ForeignKey(
        Product,
        related_name='consume_product',
        on_delete=models.CASCADE, #models.PROTECT,
    )
    type = models.CharField(
        max_length=30,
        choices=settings.PRODUCT_TYPES,
        blank=settings.SHOPPING,
    )

    class Meta:
        ordering = ('-id',)
        verbose_name = 'Consumption Action'
        verbose_name_plural = 'Consumption Actions'
        get_latest_by = 'created'

    def __str__(self):
        return self.profile.user.username + "'s consumption action"

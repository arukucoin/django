# -*- coding: utf-8 -*-
from rest_framework import serializers

from .models import *

class RewardActionSerializer(serializers.ModelSerializer):

    class Meta:
        model = RewardAction
        fields = ('coin', 'type', 'invitation_user', 'created')
        read_only_fields = ('coin', 'type', 'invitation_user', 'created')


class ConsumptionActionSerializer(serializers.ModelSerializer):
    #product = BulkProductSerializer(read_only=True)
    #brandImgUrl = serializers.CharField(source='product.brand.brandUrl', read_only=True)
    brandName = serializers.CharField(source='product.brand.name', read_only=True)
    brandAnnotation = serializers.CharField(source='product.brand.annotation', read_only=True)
    productName = serializers.CharField(source='product.name', read_only=True)
    productImgUrl = serializers.CharField(source='product.imageUrl', read_only=True)

    class Meta:
        model = ConsumptionAction
        fields = ('coin', 'type', 'brandName', 'brandAnnotation', 'productName', 'productImgUrl', 'created')

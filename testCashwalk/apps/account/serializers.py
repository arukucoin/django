# -*- coding: utf-8 -*-
from django.contrib.auth import get_user_model
from django.conf import settings
from django.contrib.auth.password_validation import validate_password

from rest_framework import serializers

from .models import *


class ChangePasswordSerializer(serializers.Serializer):
    old_password = serializers.CharField(required=True)
    new_password = serializers.CharField(required=True)

    def validate_new_password(self, value):
        validate_password(value)
        return value


class EmailRegistrationSerializer(serializers.Serializer):
    email = serializers.EmailField(required=True)
    username = serializers.CharField(max_length=50, required=True)
    password = serializers.CharField(max_length=50, required=True)

    def validate(self, data):
        return data


class SocialUserSerializer(serializers.Serializer):
    account_type = serializers.CharField(max_length=255, required=True)
    social_uid = serializers.CharField(max_length=255, required=True)

    def validate(self, data):
        return data


class SocialRegistrationSerializer(serializers.Serializer):
    account_type = serializers.CharField(max_length=255, required=True)
    social_uid = serializers.CharField(max_length=255, required=True)

    def validate(self, data):
        return data


class ProfileImageSerializer(serializers.Serializer):
    image = serializers.ImageField(required=True, allow_empty_file=False)

    def validate(self, data):
        return data

class InviteSerialization(serializers.Serializer):
    code = serializers.CharField(max_length=255, required=True)

    def validate(self, data):
        return data


class ProfileSerializer(serializers.ModelSerializer):
    username = serializers.CharField(source='user.username')
    coin = serializers.IntegerField(source='wallet.current_total_coin', read_only=True)
    today_rewarded_step_coin = serializers.IntegerField(source='wallet.today_rewarded_step_coin', read_only=True)
    video_coin_published_date =  serializers.DateTimeField(source='wallet.last_video_coin_published', read_only=True)

    class Meta:
        model = Profile
        fields = ('account_type', 'username', 'email', 'profileUrl', 'weight', 'height', 'invitation_code', 'invitation_count', 'date_joined', 'coin', 'today_rewarded_step_coin', 'video_coin_published_date', 'gender')
        read_only_fields = ('account_type', 'email', 'profileUrl', 'invitation_code', 'date_joined', 'coin', 'today_rewarded_step_coin', 'video_coin_published_date') #'invited_friends',)
        #editable(all optional) = username, weight, height, gender

    # def __init__(self, *args, **kwargs):
    #     self.Meta.fields = list(self.Meta.fields)
    #     self.Meta.fields.append('isMan')
    #     super(ProfileSerializer, self).__init__(*args, **kwargs)

    def update(self, instance, validated_data):
        instance.weight = validated_data.get('weight', instance.weight)
        instance.height = validated_data.get('height', instance.height)
        instance.gender = validated_data.get('gender', instance.gender)
        instance.save()

        user = validated_data.get('user', None)
        if user is not None:
            instance.user.username = user.get('username', instance.user.username)
            instance.user.save()
        return instance


# class ChangePasswordSerializer(serializers.Serializer):
#     old_password = serializers.CharField(required=True)
#     new_password = serializers.CharField(required=True)
#
#     def validate(self, data):
#         # add here additional check for password strength if needed
#         if not self.context['request'].user.check_password(data.get('old_password')):
#             raise serializers.ValidationError({'error': settings.CHANGEPASSWORD_ERRMESSAGE})
#
#         return data
#
#     def update(self, instance, validated_data):
#         instance.set_password(validated_data['new_password'])
#         instance.save()
#         return instance
#
#     def create(self, validated_data):
#         pass
#
#     @property
#     def data(self):
#         # just return success dictionary. you can change this to your need, but i dont think output should be user data after password change
#         return {'Success': True}

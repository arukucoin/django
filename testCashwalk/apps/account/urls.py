# -*- coding: utf-8 -*-
from django.urls import path, include

from .views import *

# v1/account/
urlpatterns = [
    # ref: https://django-rest-auth.readthedocs.io/en/latest/api_endpoints.html
    # email/login <POST: email, password>, email/registration <POST: username, password1, password2, email>

    # path('email/', include('rest_auth.urls')),
    # path('email/registration', EmailRegistration.as_view()),
    # path('password', ChangePassword.as_view()),

    path('social/login', SocialAccountLogin.as_view()),
    path('social/registration', SocialAccountRegistration.as_view()),

    path('profile', ProfileDetailEdit.as_view()),
    path('profile/image', ProfileImage.as_view()),

    path('tickets', TicketList.as_view()), # used=True or False
    path('invite/<slug:usercode>', InviteFriend.as_view()),

    path('logout', UserLogout.as_view()),
]

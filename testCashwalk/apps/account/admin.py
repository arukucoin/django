# -*- coding: utf-8 -*-
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.models import Group
from django.contrib.auth import get_user_model

from .models import *

class UserAdmin(BaseUserAdmin):
    model = get_user_model()
    list_display = ['username', 'account_type', 'email', 'gender', 'invitation_code', 'invitation_count', 'date_joined', 'is_active']
    fieldsets = (
        (None, {'fields': ('account_type', 'social_uid')}),
    ) + BaseUserAdmin.fieldsets
    add_fieldsets = BaseUserAdmin.add_fieldsets + (
        (None, {
            'fields': ('email',),
        }),
    )

    def invitation_code(self, obj):
        return obj.profile.invitation_code

    def invitation_count(self, obj):
        return obj.profile.invitation_count()

    def gender(self, obj):
        return obj.profile.gender

admin.site.register(get_user_model(), UserAdmin)
admin.site.unregister(Group)

class ProfileInline(admin.StackedInline):
    model = Profile
    readonly_fields = ['invitation_code', 'invitation_count']
    can_delete = False

class ProfileAdmin(UserAdmin):
	inlines = (ProfileInline,)

admin.site.unregister(get_user_model())
admin.site.register(get_user_model(), ProfileAdmin)


# class DeviceAdmin(admin.ModelAdmin):
#     model = Device
#     list_display = ['__str__', 'device_type', 'device_token', 'device_name', 'app_version', 'aws_sns_arn']
#     readonly_fields = ['aws_sns_arn']
#     ordering = ('-pk',)
#
# admin.site.register(Device, DeviceAdmin)

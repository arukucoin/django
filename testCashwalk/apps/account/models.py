# -*- coding: utf-8 -*-
from django.db import models
from django.conf import settings
from django.dispatch import receiver
from django.db.models.signals import post_save
from django.contrib.postgres.fields import ArrayField
from django.contrib.auth.models import (User, AbstractUser, AbstractBaseUser, PermissionsMixin, UserManager)

from testCashwalk.utils import *

from .managers import CustomUserManager

import uuid

from testCashwalk.apps.wallet.models import Wallet
from testCashwalk.apps.action.models import RewardAction, ConsumptionAction
from testCashwalk.apps.reward.models import Ticket

from testCashwalk.utils import *

path_and_rename_profile = PathAndRename("profile_image")


class CustomUser(AbstractUser):
    EMAIL = 'Email'
    LINE = 'Line'
    FACEBOOK = 'Facebook'

    ACCOUNT_TYPES = (
        (EMAIL, 'Email'),
        (LINE, 'Line'),
        (FACEBOOK, 'Facebook'),
    )

    email = models.EmailField(
        unique=True
    )
    username = models.CharField(
        max_length=50,
        blank=True,
        null=True,
        default="",
    )
    account_type = models.CharField(
        max_length=20,
        choices=ACCOUNT_TYPES,
        default=EMAIL,
    )
    social_uid = models.CharField(
        max_length=255,
        blank=True,
        null=True,
    )
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username',]

    objects = CustomUserManager()

    def __str__(self):
        return self.email

    class Meta:
        ordering = ('-id',)
        verbose_name = 'User'
        verbose_name_plural = 'Users'


@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def created_new_user(sender, created, instance, **kwargs):
    if created:
        new_profile = Profile.objects.create(user=instance)
        new_profile.save()

        if settings.REGISTRATION_REWARD_COIN > 0:
            new_profile.publish_reward_action(coin=settings.REGISTRATION_REWARD_COIN,
                type=settings.REGISTRATION_REWARD)

        new_wallet = Wallet.objects.create(
            profile=new_profile,
            bonus_coin=settings.REGISTRATION_REWARD_COIN,
        )
        new_wallet.reset_video_date()
        # new_wallet.last_video_coin_published = get_yesterday()
        new_wallet.save()

        # new_device = Device.objects.create(
        #     profile=new_profile,
        # )
        # new_device.save()


class Profile(models.Model):
    MALE = "Male"
    FEMALE = "Female"

    GENDER = (
        (MALE, "Male"),
        (FEMALE, "Female")
    )

    user = models.OneToOneField(
        settings.AUTH_USER_MODEL,
		related_name='profile',
        on_delete=models.CASCADE, #models.PROTECT,
    )
    profile_image = models.ImageField(
        upload_to=path_and_rename_profile,
        blank=True,
        null=True,
    )
    weight = models.PositiveSmallIntegerField(default=60)
    height = models.PositiveSmallIntegerField(default=170)
    gender = models.CharField(
        max_length=20,
        choices=GENDER,
        default=MALE,
    )
    invitation_code = models.CharField(
        max_length=6,
        unique=True,
        editable=False,
        default=get_inviation_code,
        verbose_name='User Invitation Code Identifier',
    )
    invitation_profiles = ArrayField(
        models.PositiveIntegerField(),
        default=list,
        blank=True,
    )

    class Meta:
        ordering = ('-id',)
        verbose_name = 'Profile'
        verbose_name_plural = 'Profiles'

    def __str__(self):
        return self.user.username + "'s profile"

    @property
    def email(self):
        if self.user.email.split('@')[1] == settings.DEFAULT_USER_EMAIL_DOMAIN[1:]:
            return ""
        return self.user.email

    def invitation_count(self):
        return len(self.invitation_profiles)

    def account_type(self):
        return self.user.account_type

    def date_joined(self):
        return self.user.date_joined

    def delete_profile_image(self):
        self.profile_image.delete()
        self.save()

    def set_profile_image(self, image):
        self.profile_image = image
        self.save()

    def profileUrl(self):
        if self.profile_image:
            if settings.DEBUG == False:
                # return settings.AWS_CF_DOMAIN + self.profile_image.name
                return self.profile_image.url
            else:
                return self.profile_image.url
        return ""

    def publish_reward_action(self, coin, type, invite_username=""):
        RewardAction.objects.create(
            profile=self,
            coin=coin,
            type=type,
            invitation_user=invite_username,
        )

    def publish_consumption_action(self, product):
        ConsumptionAction.objects.create(
            profile=self,
            type=product.product_type,
            coin=product.price,
            product=product,
        )

    def publish_ticket(self, product, gift_url):
        Ticket.objects.create(
            product=product,
            profile=self,
            ticket_url=gift_url,
        )


# class Device(models.Model):
#
#     IOS = 'iOS'
#     ANDROID = 'Android'
#     EMPTY = "Empty"
#
#     DEVICE_TYPES = (
#         (IOS, "iOS"),
#         (ANDROID, "Android"),
#         (EMPTY, "Empty"),
#     )
#
#     profile = models.OneToOneField(
#         Profile,
#         related_name='device',
#         on_delete=models.CASCADE,
#     )
#
#     device_type = models.CharField(
#         max_length=20,
#         choices=DEVICE_TYPES,
#         default=EMPTY,
#     )
#     device_token = models.CharField(
#         max_length=255,
#         blank=True,
#     )
#     device_name = models.CharField(
#         max_length=255,
#         blank=True,
#     )
#     app_version = models.CharField(
#         max_length=50,
#         blank=True,
#     )
#     aws_sns_arn = models.CharField(
#         max_length=255,
#         blank=True,
#     )
# 	#badge_count = models.PositiveIntegerField(default=0, editable=False)
#
#     class Meta:
#         ordering = ('-id',)
#         verbose_name = 'Device'
#         verbose_name_plural = 'Devices'
#
#     def __str__(self):
#         return self.profile.user.username + "'s device"

	# def increase_badge_count(self):
	# 	self.badge_count += 1
	# 	self.save()
	# 	return self.badge_count
    #
	# def reset_badge_count(self):
	# 	self.badge_count = 0
	# 	self.save()
    #
	# def reset_device_info(self):
	# 	self.device_token = ''
	# 	self.device_os = None
	# 	self.device_name = ''
	# 	self.app_version = ''
	# 	self.save()
    #
	# def add_endpoint(self):
	# 	aws_sns = boto.sns.connect_to_region(settings.AWS_SNS_REGION_NAME)
    #
	# 	if 'iOS' in self.device_os:
	# 		result = aws_sns.create_platform_endpoint(platform_application_arn=settings.AWS_SNS_APNS, token=self.device_token, custom_user_data=self.user.email)
	# 		#result = aws_sns.create_platform_endpoint(platform_application_arn=settings.AWS_SNS_APNS_SANDBOX, token=self.device_token, custom_user_data=self.user.email) # for iOS APNS (SANDBOX)
	# 		self.aws_sns_arn = result['CreatePlatformEndpointResponse']['CreatePlatformEndpointResult']['EndpointArn']
    #
	# 	if 'android' in self.device_os:
	# 		result = aws_sns.create_platform_endpoint(platform_application_arn=settings.AWS_SNS_GCM, token=self.device_token, custom_user_data=self.user.email)
	# 		self.aws_sns_arn = result['CreatePlatformEndpointResponse']['CreatePlatformEndpointResult']['EndpointArn']
    #
	# 	self.save()
    #
	# def delete_endpoint(self):
    #
	# 	if len(self.aws_sns_arn) > 0:
    #
	# 		aws_sns = boto.sns.connect_to_region(settings.AWS_SNS_REGION_NAME)
	# 		aws_sns.delete_endpoint(endpoint_arn=self.aws_sns_arn)
	# 		self.aws_sns_arn=''
	# 		self.save()

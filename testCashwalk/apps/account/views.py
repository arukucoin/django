# -*- coding: utf-8 -*-
from django.contrib.auth import get_user_model
from django.core.exceptions import ObjectDoesNotExist
from django.shortcuts import get_object_or_404
from django.http import Http404
from django.conf import settings
from django.db.models import Q
from django.http import JsonResponse

from rest_framework import parsers, renderers, generics, status, authentication
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.authtoken.models import Token
from rest_framework.permissions import IsAuthenticated
from rest_framework.exceptions import NotFound

from testCashwalk.utils import ResultPagination
from testCashwalk.apps.reward.models import Ticket
from testCashwalk.apps.reward.serializers import TicketSerializer
from testCashwalk.apps.action.models import RewardAction

from testCashwalk.utils import *

from datetime import datetime, timedelta

from .models import Profile
from .serializers import *


# class ChangePassword(generics.UpdateAPIView):
#     parser_classes = (parsers.JSONParser,)
#     renderer_classes = (renderers.JSONRenderer,)
#     authentication_classes = (authentication.TokenAuthentication,)
#     permission_classes = (IsAuthenticated,)
#     serializer_class = ChangePasswordSerializer
#
#     def get_object(self, queryset=None):
#         return self.request.user


# class ChangePassword(APIView):
#     parser_classes = (parsers.JSONParser,)
#     renderer_classes = (renderers.JSONRenderer,)
#     authentication_classes = (authentication.TokenAuthentication,)
#     permission_classes = (IsAuthenticated,)
#     serializer_class = ChangePasswordSerializer
#
#     def put(self, request, *args, **kwargs):
#         serializer = self.serializer_class(data=request.data)
#
#         if serializer.is_valid():
#             old_password = serializer.validated_data['old_password']
#             new_password = serializer.validated_data['new_password']
#
#             if not self.request.user.check_password(old_password):
#                 return Response({"error": settings.CHANGEPASSWORD_ERRMESSAGE},
#                     status=status.HTTP_400_BAD_REQUEST)
#
#             self.request.user.set_password(new_password)
#             self.request.user.save()
#             return Response(status=status.HTTP_204_NO_CONTENT)
#
#         return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
#
#
# class EmailRegistration(APIView):
#     parser_classes = (parsers.JSONParser,)
#     renderer_classes = (renderers.JSONRenderer,)
#     serializer_class = EmailRegistrationSerializer
#
#     def post(self, request, format=None):
#         serializer = self.serializer_class(data=request.data)
#
#         if serializer.is_valid():
#             email = serializer.validated_data['email']
#             username = serializer.validated_data['username']
#             password = serializer.validated_data['password']
#
#             try:
#                 user = get_user_model().objects.get(email=email)
#                 return Response({'error': settings.EMAIL_REGISTRATIOM_ERRMESSAGE_1}, status=status.HTTP_400_BAD_REQUEST)
#             except ObjectDoesNotExist:
#                 new_user = get_user_model().objects.create_user(
#                     email=email,
#                     password=password,
#                     username=username,
#                     account_type = get_user_model().EMAIL,
#                 )
#                 token, created = Token.objects.get_or_create(user=new_user)
#                 return Response({'token': token.key}, status=status.HTTP_200_OK)
#
#         return Response(status=status.HTTP_400_BAD_REQUEST)


class SocialAccountLogin(APIView):
    parser_classes = (parsers.JSONParser,)
    renderer_classes = (renderers.JSONRenderer,)
    serializer_class = SocialUserSerializer

    def post(self, request, format=None):
        serializer = self.serializer_class(data=request.data)

        if serializer.is_valid():
            uid = serializer.validated_data['social_uid']
            type = serializer.validated_data['account_type']

            user = get_object_or_404(
                get_user_model(),
                account_type=type,
                social_uid=uid,
            )
            token, created = Token.objects.get_or_create(user=user)
            if created == False:
                Token.objects.get(user_id=user.id).delete()
                token, created = Token.objects.get_or_create(user=user)

            return Response({'token': token.key}, status=status.HTTP_200_OK)

        return Response(status=status.HTTP_400_BAD_REQUEST)


class SocialAccountRegistration(APIView):
    parser_classes = (parsers.JSONParser,)
    renderer_classes = (renderers.JSONRenderer,)
    serializer_class = SocialRegistrationSerializer

    def post(self, request, format=None):
        serializer = self.serializer_class(data=request.data)

        if serializer.is_valid():
            type = serializer.validated_data['account_type']
            uid = serializer.validated_data['social_uid']
            email = get_temp_email()

            new_user = get_user_model().objects.create_social_account(
                account_type=type, social_uid=uid, email=email
            )
            token = Token.objects.create(user=new_user)
            return Response({'token': token.key}, status=status.HTTP_201_CREATED)

        return Response(status=status.HTTP_400_BAD_REQUEST)


class UserLogout(APIView):
    parser_classes = (parsers.JSONParser,)
    renderer_classes = (renderers.JSONRenderer,)
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get(self, request, format=None):
        Token.objects.get(user=self.request.user).delete()
        return Response(status=status.HTTP_200_OK)


class ProfileDetailEdit(generics.RetrieveUpdateAPIView):
    parser_classes = (parsers.JSONParser,)
    renderer_classes = (renderers.JSONRenderer,)
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    serializer_class = ProfileSerializer

    def get_object(self):
        return self.request.user.profile

    def put(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)


class ProfileImage(generics.CreateAPIView):
    parser_classes = (parsers.JSONParser, parsers.MultiPartParser,)
    renderer_classes = (renderers.JSONRenderer,)
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    serializer_class = ProfileImageSerializer

    def create(self, request, *args, **kwargs):
        serializer = ProfileImageSerializer(data=self.request.data)
        if serializer.is_valid():
            img = serializer.validated_data.get('image')
            profile = get_object_or_404(Profile, user=self.request.user)
            profile.set_profile_image(img)
            return Response(status=status.HTTP_201_CREATED)

        return Response(status=status.HTTP_400_BAD_REQUEST)


class InviteFriend(APIView):
    parser_classes = (parsers.JSONParser,)
    renderer_classes = (renderers.JSONRenderer,)
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    serializer_class = InviteSerialization

    def get(self, request, usercode, format=None):
        try:
            profile = Profile.objects.get(invitation_code=usercode)

            if profile.id == self.request.user.profile.id:
                return Response({'error': settings.INVITEDFRIEND_ERRMESSAGE_1},
                    status.HTTP_400_BAD_REQUEST)

            if profile.invitation_count() >= settings.MAXIMUM_INVITATION_PEOPLE:
                return Response({'error': settings.INVITEDFRIEND_ERRMESSAGE_2},
                    status.HTTP_400_BAD_REQUEST)

            if self.request.user.profile.id in profile.invitation_profiles:
                return Response({'error': settings.INVITEDFRIEND_ERRMESSAGE_3},
                    status.HTTP_400_BAD_REQUEST)

            profile.wallet.bonus_coin += settings.INVITATION_REWARD_COIN
            profile.wallet.save()
            profile.invitation_profiles.append(self.request.user.profile.id)
            profile.save()

            profile.publish_reward_action(coin=settings.INVITATION_REWARD_COIN,
                type=settings.FRIEND_INVITATION,
                invite_username=self.request.user.username
            )
            return Response(status=status.HTTP_200_OK)

        except ObjectDoesNotExist:
            return Response({'error': settings.INVITEDFRIEND_ERRMESSAGE_1}, status.HTTP_404_NOT_FOUND)
            #raise NotFound({'error': settings.INVITEDFRIEND_ERRMESSAGE_1}, code=404)
            #err = json.dumps({'error': settings.INVITEDFRIEND_ERRMESSAGE_1})
            #raise NotFound(err, code=404)
            #raise Http404


class TicketList(generics.ListAPIView):
    parser_classes = (parsers.JSONParser,)
    renderer_classes = (renderers.JSONRenderer,)
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    pagination_class =  ResultPagination
    serializer_class = TicketSerializer

    def get_queryset(self):
        query_date = datetime.now() - timedelta(days=7)

        if self.request.query_params.get('used', None) == 'True':
            return self.request.user.profile.tickets.filter(Q(published_date__lte=query_date, is_used=False) | Q(is_used=True))#.order_by('id')
        else:
            return self.request.user.profile.tickets.filter(published_date__gte=query_date, is_used=False)#.order_by('id')


# def post(self, request, format=None):
#     serializer = self.serializer_class(data=request.data)
#
#     if serializer.is_valid():
#         usercode = serializer.validated_data['code']
#
#         try:
#             profile = Profile.objects.get(invitation_code=usercode)
#
#             if profile.id == self.request.user.profile.id:
#                 return Response({'error': settings.INVITEDFRIEND_ERRMESSAGE_1},
#                     status.HTTP_400_BAD_REQUEST)
#
#             if profile.invitation_count() >= 50:
#                 return Response({'error': settings.INVITEDFRIEND_ERRMESSAGE_2},
#                     status.HTTP_400_BAD_REQUEST)
#
#             if self.request.user.profile.id in profile.invitation_profiles:
#                 return Response({'error': settings.INVITEDFRIEND_ERRMESSAGE_3},
#                     status.HTTP_400_BAD_REQUEST)
#
#             profile.wallet.bonus_coin += settings.INVITATION_REWARD_COIN
#             profile.wallet.save()
#             profile.invitation_profiles.append(self.request.user.profile.id)
#             profile.save()
#
#             RewardAction.objects.create(
#                 profile=profile,
#                 coin=settings.INVITATION_REWARD_COIN,
#                 reward_type=settings.FRIEND_INVITATION,
#                 invitation_user=self.request.user.username,
#             )
#             return Response(status=status.HTTP_200_OK)
#
#         except ObjectDoesNotExist:
#             raise NotFound({'error': settings.INVITEDFRIEND_ERRMESSAGE_1}, code=404)
#
#     return Response(status=status.HTTP_400_BAD_REQUEST)

from django.contrib import admin

from .models import *


class GiftAdmin(admin.ModelAdmin):
    model = Gift
    list_display = ['product_uid', 'url', 'expiration_date', 'is_published', 'ticket_uid']
    #ordering = ('-pk',)

admin.site.register(Gift, GiftAdmin)

from django.db import models

import uuid


class Gift(models.Model):
    product_uid = models.UUIDField(
        unique=False,
        editable=True,
        default=uuid.uuid4,
        verbose_name='Product identifier',
    )
    url = models.CharField(default="", max_length=255)
    is_published = models.BooleanField(default=False)
    expiration_date = models.DateTimeField(
        auto_now_add=False,
        auto_now=False,
        blank=True,
    )
    ticket_uid = models.CharField(default="", blank=True, max_length=255)

    class Meta:
        ordering = ('-id',)
        verbose_name = "Gift"
        verbose_name_plural = "Gifts"

    def __str__(self):
        return self.url

    def published(self, ticket_uid):
        self.is_published = True
        self.ticket_uid = ticket_uid
        self.save()

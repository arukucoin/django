# -*- coding: utf-8 -*-
from django.urls import path

from .views import *

urlpatterns = [
    path('wallet', CoinDetail.as_view()), #GET
    path('step', StepRewardCoin.as_view()), #POST
    path('video', VideoRewardCoin.as_view()), #POST
    path('offerwall', OfferwallRewardCoin.as_view()), #POST

    path('video/date/reset', VideoRewardDateReset.as_view()), #GET (only test)
]

# -*- coding: utf-8 -*-
from django.db import models
from django.core.exceptions import ObjectDoesNotExist
from django.conf import settings
from django.core.validators import MaxValueValidator, MinValueValidator
from django.utils import timezone

from datetime import datetime, timedelta

from testCashwalk.apps.action.models import RewardAction
from testCashwalk.utils import *


class UserWallet(models.Model):
    step_coin = models.PositiveIntegerField(default=0)
    bonus_coin = models.PositiveIntegerField(default=0) # 1. registration, 2. invitation
    offerwall_coin = models.PositiveIntegerField(default=0) # 1. OfferWall
    advertise_coin = models.PositiveIntegerField(default=0) # 1. Advertise

    consume_coin = models.PositiveIntegerField(default=0) # 1. Buy 2. Lottery

    class Meta:
        abstract = True


class Wallet(UserWallet):
    profile = models.OneToOneField(
        'account.Profile',
		related_name='wallet',
        on_delete=models.CASCADE, #models.PROTECT,
    )
    last_step_coin_published = models.DateTimeField(auto_now_add=True) #auto_now=True
    last_step_coin = models.PositiveSmallIntegerField(
        default=0,
        validators=[MinValueValidator(0), MaxValueValidator(settings.MAXIMUM_DAILY_STEP_COIN)]
    )
    last_video_coin_published = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ('-id',)
        verbose_name = 'Wallet'
        verbose_name_plural = 'Wallets'


    def __str__(self):
        return self.profile.user.username + "'s wallet"

    @property
    def current_total_coin(self):
        return self.total_reward_coin - self.consume_coin

    @property
    def total_reward_coin(self):
        return self.total_step_coin + self.total_etc_coin

    @property
    def total_etc_coin(self):
        return self.bonus_coin + self.offerwall_coin + self.advertise_coin

    @property
    def total_step_coin(self):
        return self.step_coin + self.last_step_coin

    def last_step_date(self):
        return localize_date(self.last_step_coin_published)

    def buy_product(self, coin):
        self.consume_coin += coin
        self.save()

    def today_rewarded_step_coin(self):
        locale_now = get_locale_now()
        last_date = self.last_step_date()

        if last_date.date() == locale_now.date():
            return self.last_step_coin
        else:
            self.step_coin += self.last_step_coin
            self.last_step_coin = 0
            self.save()
            return self.last_step_coin

    def publish_step_coin(self, coin=0):
        now = datetime.now()

        rewarded_coin = 0
        today_step_coin = self.today_rewarded_step_coin()
        last_date = self.last_step_date()

        # if last_date.date().month != get_locale_now().month or last_date.date().year != get_locale_now().year:
        #     self.create_monthly_wallet_log()

        if today_step_coin + coin <= settings.MAXIMUM_DAILY_STEP_COIN:
            self.last_step_coin_published = now
            self.last_step_coin = today_step_coin + coin
            self.save()

            rewarded_coin = coin

            if today_step_coin == 0:
                self.profile.publish_reward_action(coin=rewarded_coin, type=settings.DAILY_STEP)
            else:
                action = RewardAction.objects.filter(
                    profile=self.profile,
                    type=settings.DAILY_STEP
                ).latest()
                action.coin = self.last_step_coin
                action.date = now
                action.save()

            return rewarded_coin

        return 0

    def last_video_date(self):
        return localize_date(self.last_video_coin_published)

    def reset_video_date(self):
        self.last_video_coin_published = get_yesterday()

    def publish_video_coin(self, coin):
        locale_now = get_locale_now()
        last_date = self.last_video_date()

        if last_date.date() == locale_now.date():
            return 0

        self.advertise_coin += coin
        self.last_video_coin_published = timezone.now()
        self.save()
        return coin

    def publish_offerwall_coin(self, coin):
        self.offerwall_coin += coin
        self.save()
        return coin

    # def create_monthly_wallet_log(self):
    #     WalletLog.objects.create(
    #         wallet=self,
    #         step_coin=self.total_step_coin,
    #         bonus_coin=self.bonus_coin,
    #         offerwall_coin=self.offerwall_coin,
    #         advertise_coin=self.advertise_coin,
    #         consume_coin=self.consume_coin,
    #         last_step_published=self.last_step_coin_published,
    #     )


# class WalletLog(UserWallet):
#     wallet = models.ForeignKey(
#         Wallet,
#         related_name='logs',
#         on_delete=models.CASCADE,
#     )
#     last_step_published = models.DateTimeField()
#     created = models.DateTimeField(auto_now=True)
#
#     class Meta:
#         ordering = ('-id',)
#         verbose_name = 'Wallet Log'
#         verbose_name_plural = 'Wallet Logs'
#
#     @property
#     def total_coin(self):
#         return (self.step_coin + self.bonus_coin + self.offerwall_coin + self.advertise_coin) - self.consume_coin
#
#     def log_month(self):
#         return str(self.last_step_published.date().year) + "." + str(self.last_step_published.date().month)
#
#     def last_rewarded_step_date(self):
#         return str(self.last_step_published.date().year) + "." \
#             +  str(self.last_step_published.date().month) + "." + str(self.last_step_published.date().day)
#
#     def created_date(self):
#         return str(self.created.year) + "." + str(self.created.month) + "." \
#             + str(self.created.day) + " " + str(self.created.hour) + ":" + str(self.created.minute)
#
#     def __str__(self):
#         return self.wallet.profile.user.username + "'s wallet Log"

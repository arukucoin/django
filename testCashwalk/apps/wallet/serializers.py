# -*- coding: utf-8 -*-
from django.conf import settings

from rest_framework import serializers

from .models import *


class StepCoinRewardSerializer(serializers.Serializer):
    coin = serializers.IntegerField(min_value=0, max_value=100, required=True)

    def validte(self, date):
        return data


class VideoCoinRewardSerializer(serializers.Serializer):
    coin = serializers.IntegerField(min_value=0, max_value=100, required=True)

    def validte(self, date):
        return data

class OfferwallCoinRewardSerializer(serializers.Serializer):
    coin = serializers.IntegerField(min_value=0, required=True)

    def validte(self, date):
        return data


class CoinSerializer(serializers.ModelSerializer):

    class Meta:
        model = Wallet
        fields = ('current_total_coin', 'total_step_coin', 'total_etc_coin', 'consume_coin',)
        read_only_fields = ('current_total_coin', 'total_step_coin', 'total_etc_coin', 'consume_coin',)

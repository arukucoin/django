# -*- coding: utf-8 -*-
from django.contrib import admin

from .models import *

class WalletAdmin(admin.ModelAdmin):
    model = Wallet
    list_display = ['__str__', 'current_total_coin', 'total_reward_coin', 'total_step_coin', \
        'step_coin', 'last_step_coin', 'bonus_coin', 'offerwall_coin', 'advertise_coin', 'consume_coin', 'last_step_coin_published', 'last_video_coin_published']
    ordering = ('-pk',)

admin.site.register(Wallet, WalletAdmin)


# class WalletLogAdmin(admin.ModelAdmin):
#     model = WalletLog
#     list_display = ['__str__', 'log_month', 'total_coin', 'consume_coin', 'last_rewarded_step_date', 'created_date']
#     readonly_fields = ['__str__', 'step_coin', 'bonus_coin', 'offerwall_coin', 'advertise_coin', 'consume_coin', 'last_step_published', 'created']
#     ordering = ('-pk',)
#
# admin.site.register(WalletLog, WalletLogAdmin)

# -*- coding: utf-8 -*-
from django.conf import settings

from rest_framework import parsers, renderers, generics, status, authentication
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated

from testCashwalk.utils import ResultPagination

from .models import *
from .serializers import *


class StepRewardCoin(APIView):
    parser_classes = (parsers.JSONParser,)
    renderer_classes = (renderers.JSONRenderer,)
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    serializer_class = StepCoinRewardSerializer

    def post(self, request, format=None):
        serializer = self.serializer_class(data=request.data)

        if serializer.is_valid():
            coin = serializer.validated_data['coin']

            if coin <= settings.STEP_COIN_AD_INTERVAL:
                rewarded_coin = self.request.user.profile.wallet.publish_step_coin(coin)
                return Response({'coin': rewarded_coin,
                            'total_coin': self.request.user.profile.wallet.current_total_coin},
                            status=status.HTTP_200_OK)

        return Response(status=status.HTTP_400_BAD_REQUEST)


class VideoRewardCoin(APIView):
    parser_classes = (parsers.JSONParser,)
    renderer_classes = (renderers.JSONRenderer,)
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    serializer_class = VideoCoinRewardSerializer

    def post(self, request, format=None):
        serializer = self.serializer_class(data=request.data)

        if serializer.is_valid():
            coin = serializer.validated_data['coin']
            rewarded_coin = self.request.user.profile.wallet.publish_video_coin(coin)

            if rewarded_coin > 0:
                self.request.user.profile.publish_reward_action(coin=rewarded_coin, type=settings.VIDEO_REWARD)
                return Response({'coin': rewarded_coin,
                    'total_coin': self.request.user.profile.wallet.current_total_coin},
                    status=status.HTTP_200_OK)

            return Response({'error': settings.VIDEOCOIN_ERRMESSAGE_1}, status=status.HTTP_400_BAD_REQUEST)

        return Response(status=status.HTTP_400_BAD_REQUEST)


class OfferwallRewardCoin(APIView):
    parser_classes = (parsers.JSONParser,)
    renderer_classes = (renderers.JSONRenderer,)
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    serializer_class = OfferwallCoinRewardSerializer

    def post(self, request, format=None):
        serializer = self.serializer_class(data=request.data)

        if serializer.is_valid():
            coin = serializer.validated_data['coin']
            rewarded_coin = self.request.user.profile.wallet.publish_offerwall_coin(coin)

            if rewarded_coin > 0:
                self.request.user.profile.publish_reward_action(coin=rewarded_coin, type=settings.OFFERWALL_REWARD)
                return Response({'coin': rewarded_coin,
                    'total_coin': self.request.user.profile.wallet.current_total_coin},
                    status=status.HTTP_200_OK)

            return Response({'error': settings.OFFERWALL_ERRMESSAGE_1}, status=status.HTTP_400_BAD_REQUEST)

        return Response(status=status.HTTP_400_BAD_REQUEST)


class CoinDetail(generics.RetrieveAPIView):
    parser_classes = (parsers.JSONParser,)
    renderer_classes = (renderers.JSONRenderer,)
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    serializer_class = CoinSerializer

    def get_object(self):
        return self.request.user.profile.wallet


class VideoRewardDateReset(APIView):
    parser_classes = (parsers.JSONParser,)
    renderer_classes = (renderers.JSONRenderer,)
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get(self, request, format=None):
        self.request.user.profile.wallet.reset_video_date()
        self.request.user.profile.wallet.save()
        return Response(status=status.HTTP_200_OK)

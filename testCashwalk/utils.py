# -*- coding: utf-8 -*-
import os
import uuid

from django.utils.deconstruct import deconstructible
from django.utils import timezone
from django.conf import settings

from rest_framework.response import Response
from rest_framework import pagination
from rest_framework.views import exception_handler
from rest_framework.utils.serializer_helpers import ReturnDict

from datetime import datetime, timedelta

import pytz
import uuid


@deconstructible
class PathAndRename(object):
    def __init__(self, sub_path):
        self.path = sub_path

    def __call__(self, instance, filename):
        ext = filename.split('.')[-1]
        # set filename as random string
        filename = '{}.{}'.format(uuid.uuid4().hex, ext)
        # return the whole path to the file
        return os.path.join(self.path, filename)


class ResultPagination(pagination.PageNumberPagination):

    def get_paginated_response(self, data):
        return Response({
            'results': data
        })

def get_yesterday():
    return timezone.now() - timedelta(days=1)
    #return datetime.now() - timedelta(days=1)

def get_locale_now():
    now = datetime.now(tz=pytz.timezone('Asia/Tokyo'))
    return now

def localize_date(date):
    return date.astimezone(pytz.timezone('Asia/Tokyo'))

def get_inviation_code(string_length=6):
    """Returns a random string of length string_length."""
    random = str(uuid.uuid4()) # Convert UUID format to a Python string.
    random = random.upper() # Make all characters uppercase.
    random = random.replace("-","") # Remove the UUID '-'.
    return random[0:string_length]

def get_temp_email():
    return uuid.uuid4().hex[:8] + settings.DEFAULT_USER_EMAIL_DOMAIN


def custom_exception_handler(exc, context):
    # Call REST framework's default exception handler first,
    # to get the standard error response.
	response = exception_handler(exc, context)
    # Now add the HTTP status code to the response.
	if response is not None:

		keys = list(response.data.keys())

		if len(response.data) == 1:

			if type(response.data) == ReturnDict:
				response.data['error'] = response.data.values()[0][0]
			else:
				response.data['error'] = response.data.values()[0]
		else:
			response.data['error'] = response.data[keys[0]][0]

		for key in keys:
			response.data.pop(key, None)

	return response

# class NextPagination(pagination.PageNumberPagination):
#
#     def get_paginated_response(self, data):
#         return Response({
#             'next': self.get_next_link(),
#             'results': data
#         })

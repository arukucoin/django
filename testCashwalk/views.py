# -*- coding: utf-8 -*-
from django.shortcuts import render
from django.http import HttpResponse
from django.conf import settings

from rest_framework import parsers, renderers, generics, status, authentication
from rest_framework.views import APIView
from rest_framework.response import Response


def ping(request):
    return HttpResponse('pong')

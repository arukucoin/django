# -*- coding: utf-8 -*-
from .base import *

DEBUG = True

ALLOWED_HOSTS = ['*']

# DATABASES = {
#     'default': {
#         'ENGINE': 'django.db.backends.sqlite3',
#         'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
#     }
# }

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'testCashwalk_dev',
        'USER': 'simon',
        'PASSWORD': '',
        'HOST': 'localhost',
        'PORT': '5432',
    }
}

#Cache
# CACHEOPS_REDIS = {
#     'host': 'localhost', # redis-server is on same machine
#     'port': 6379,        # default redis port
#     'db': 1,             # SELECT non-default redis database
#                          # using separate redis db or redis instance
#                          # is highly recommended
#
#     'socket_timeout': '3',  # connection timeout in seconds, optional
#     'password': '',     # optional
#     'unix_socket_path': '' # replaces host and port
# }
# CACHEOPS = {
#     # Enable manual caching on all other models with default timeout of an hour
#     # Use Post.objects.cache().get(...)
#     #  or Tags.objects.filter(...).order_by(...).cache()
#     # to cache particular ORM request.
#     # Invalidation is still automatic
#     '*.*': {'ops': (), 'timeout': 60*60},
#
#     # And since ops is empty by default you can rewrite last line as:
#     '*.*': {'timeout': 60*60},
#
#     # NOTE: binding signals has its overhead, like preventing fast mass deletes,
#     #       you might want to only register whatever you cache and dependencies.
# }
#
# CACHEOPS_REDIS = "redis://localhost:6379/1"
# CACHEOPS_DEFAULTS = {
#     'timeout': 60*60
# }
# CACHEOPS = {
#     '*.*': {},
# }

# django-cache
# CACHES = {
#     "default": {
#         "BACKEND": "django_redis.cache.RedisCache",
#         "LOCATION": "redis://127.0.0.1:6379/1", # 1번 DB
#         "OPTIONS": {
#             "CLIENT_CLASS": "django_redis.client.DefaultClient",
#         }
#     }
# }



# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.8/howto/static-files/

INSTALLED_APPS += ('debug_toolbar',)
MIDDLEWARE += ('debug_toolbar.middleware.DebugToolbarMiddleware',)

STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, "static")

MEDIA_URL = '/uploads/'
MEDIA_ROOT = os.path.join(BASE_DIR, 'uploaded_files')

# -*- coding: utf-8 -*-
from .base import *
from django.core.exceptions import ImproperlyConfigured

DEBUG = True #False

ALLOWED_HOSTS = ['*']

DATABASES = {

	'default': {
    	'ENGINE': 'django.db.backends.postgresql_psycopg2',
		'NAME': get_env_variable('DB_NAME'),
		'USER': get_env_variable('DB_USER'),
		'PASSWORD': get_env_variable('DB_PASSWORD'),
		'HOST': get_env_variable('DB_HOST'), # Empty for localhost through domain sockets or '127.0.0.1' for localhost through TCP.
		'PORT': '5432', # Set to empty string for default.
	}
}
	#,
	#'slave': {
		#'ENGINE': 'django.contrib.gis.db.backends.postgis',
		#'NAME': get_env_variable('DB_NAME'),
		#'USER': get_env_variable('DB_USER'),
		#'PASSWORD': get_env_variable('DB_PASSWORD'),
		#'HOST': 'replica1.c3w7ilb3ys57.ap-northeast-1.rds.amazonaws.com',
		#'PORT': '5432', # Set to empty string for default.
	#},
#}

#DATABASE_ROUTERS = ['circleboard.apps.utils.PrimaryReplicaRouter']

#CACHES = {
    #'default': {
        #'BACKEND': 'redis_cache.RedisCache',
        #'LOCATION': '/var/run/redis/redis.sock',
    #},
#}

#INSTALLED_APPS += ('s3_folder_storage',)

#Amazon AWS settings
AWS_ACCESS_KEY_ID = get_env_variable('AWS_ACCESS_KEY_ID')
AWS_SECRET_ACCESS_KEY = get_env_variable('AWS_SECRET_ACCESS_KEY')
#
# # AWS S3 storage settings
INSTALLED_APPS += ('s3_folder_storage',)
AWS_STORAGE_BUCKET_NAME = get_env_variable('AWS_STORAGE_BUCKET_NAME')
#
DEFAULT_FILE_STORAGE = 's3_folder_storage.s3.DefaultStorage'
DEFAULT_S3_PATH = 'media'
#
STATICFILES_STORAGE = 's3_folder_storage.s3.StaticStorage'
STATIC_S3_PATH = 'static'

# AWS CloudFront URL
AWS_CF_DOMAIN = get_env_variable('AWS_CF_DOMAIN')


STATIC_ROOT = '/%s/' % STATIC_S3_PATH
STATIC_URL = 'https://%s.s3.amazonaws.com/static/' % AWS_STORAGE_BUCKET_NAME
#
MEDIA_ROOT = '/%s/' % DEFAULT_S3_PATH
MEDIA_URL = 'https://%s.s3.amazonaws.com/media/' % AWS_STORAGE_BUCKET_NAME
#
ADMIN_MEDIA_PREFIX = STATIC_URL + 'admin/'
AWS_QUERYSTRING_AUTH = False
#
AWS_HEADERS = {
	'Expires': 'Thu, 31 Dec 2099 20:00:00 GMT',
	'Cache-Control': 'max-age=94608000',
}

# -*- coding: utf-8 -*-

DEFAULT_URL = "http://stepper.snippet.kr"
CS_URL = "steppercs@gmail.com"
IOS_STORE_URL = "#"
ANDROID_STORE_URL = "#"

# Constants
DEFAULT_USER_EMAIL_DOMAIN = '@test.kr'

# Coins Limitation
MAXIMUM_DAILY_STEP_COIN = 100
COIN_PER_STEP = 100
REGISTRATION_REWARD_COIN = 100
INVITATION_REWARD_COIN = 100
STEP_COIN_AD_INTERVAL = 10

# Invitation Limitation
MAXIMUM_INVITATION_PEOPLE = 50

# Gift Expiration
GIFT_EXPIRATION_DAYS = 8

# Product Types
SHOPPING = "Shopping"
LOTTERY = "Lottery"

PRODUCT_TYPES = (
    (SHOPPING, "Shopping"),
    (LOTTERY, "Lottery"),
)

# Reward Types
DAILY_STEP = "Step"
FRIEND_INVITATION = "Invitation"
REGISTRATION_REWARD = "Registration"
OFFERWALL_REWARD = "Offerwall"
VIDEO_REWARD = "Video"

REWARD_TYPES = (
    (DAILY_STEP, "Step"),
    (FRIEND_INVITATION, "Invitation"),
    (VIDEO_REWARD, "Video"),
    (REGISTRATION_REWARD, "Registration"),
    (OFFERWALL_REWARD, "Offerwall"),
)

# ErrorMessages
INVITEDFRIEND_ERRMESSAGE_1 = u"正しいコードを入力してください."
#INVITEDFRIEND_ERRMESSAGE_1 = u"잘못된 초대코드입니다."
INVITEDFRIEND_ERRMESSAGE_2 = u"招待承認回数が50人を超えました.他の招待人のコードを入力したください."
#INVITEDFRIEND_ERRMESSAGE_2 = u"해당유저의 초대승인수가 50명을 넘었습니다. 다른사람의 추천코드를 입력해주세요."
INVITEDFRIEND_ERRMESSAGE_3 = u"すでに招待承認されたユーザーのコードです.他の招待人のコードを入力したください."
#INVITEDFRIEND_ERRMESSAGE_3 = u"이미 추천완료된 유저입니다."

EMAIL_REGISTRATIOM_ERRMESSAGE_1 = u"すでに加入したユーザーです."
#EMAIL_REGISTRATIOM_ERRMESSAGE_1 = u"이미 가입된 유저입니다."
CHANGEPASSWORD_ERRMESSAGE = u"正しくないパスワードです."
#CHANGEPASSWORD_ERRMESSAGE = u"잘못된 비밀번호입니다."
VIDEOCOIN_ERRMESSAGE_1 = u"ビデオは一日一回まで視聴できます."
#VIDEOCOIN_ERRMESSAGE_1 = u"비디오 보상형 코인은 하루에 1회만 적립가능합니다."
OFFERWALL_ERRMESSAGE_1 = u"オファーウォール"
# OFFERWALL_ERRMESSAGE_1 = u"오퍼월코인 적립이 실폐하였습니다."
